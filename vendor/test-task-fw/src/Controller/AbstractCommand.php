<?php

namespace TestTaskFW\Controller;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
abstract class AbstractCommand
{
    /**
     * Котроллер
     * @var AbstractController
     */
    private $controller;

    public function __construct(AbstractController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Контроллер
     * @return AbstractController
     */
    public function getController()
    {
        return $this->controller;
    }

    abstract public function execute();
}
