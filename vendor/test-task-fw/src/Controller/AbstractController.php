<?php

namespace TestTaskFW\Controller;

use Exception;
use TestTaskFW\App\Application;
use TestTaskFW\Http\Response;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
abstract class AbstractController
{
    /**
     * Приложение
     * @var Application
     */
    private $application;

    /**
     *
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Запускает текущую команду контроллера на выполнение
     */
    public function run()
    {
        try {
            $this->getCommand()->execute();
        } catch (Exception $ex) {
            $this->emitErrorResponse($ex);
        }
    }

    /**
     * @return AbstractCommand
     */
    abstract public function getCommand();

    abstract protected function emitErrorResponse(
            Exception $exception, $status = Response\AbstractResponse::HTTP_STATUS_BAD_REQUEST);

    /**
     * Возвращает приложение от имени которого запущен контроллер
     * @return Application
     */
    public function getApp()
    {
        return $this->application;
    }
}
