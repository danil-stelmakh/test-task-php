<?php

namespace TestTaskFW\App;

use TestTaskFW\Config\Config;
use TestTaskFW\Storage\ConnectionManager;
use TestTaskFW\Http\ServerRequest;

/**
 * Класс приложения
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Application
{
    /**
     * Конфиг приложения
     * @var Config
     */
    private $config;
    /**
     * Менеджер соединеиний
     * @var ConnectionManager
     */
    private $connectionManager;
    /**
     * Данные запроса к серверу
     * @var ServerRequest
     */
    private $request;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Конфиг приложения
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Получить менеджер соединений
     * @return ConnectionManager
     */
    public function getConnectionManager()
    {
        if (!$this->connectionManager) {
            $this->connectionManager = new ConnectionManager($this->getConfig());
        }
        return $this->connectionManager;
    }

    /**
     * Данные запроса
     * @return ServerRequest
     */
    public function getRequest()
    {
        if (!$this->request) {
            $this->request = new ServerRequest();
        }
        return $this->request;
    }
}
