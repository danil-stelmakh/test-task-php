<?php

namespace TestTaskFW\Config\Exception;

/**
 * Бросается в случае если нет файла конфига к которому мы пытаемся обратиться
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ConfigNotExistException extends ConfigException
{

}
