<?php

namespace TestTaskFW\Config\Exception;

/**
 * Бросается в случае если файл конфига вернул отличное от массива значение
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class IncorrectConfigDataException extends ConfigException
{

}
