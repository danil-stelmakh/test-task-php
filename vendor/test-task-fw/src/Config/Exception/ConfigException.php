<?php

namespace TestTaskFW\Config\Exception;

use Exception;

/**
 * Базовый эксепшен конфига
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ConfigException extends Exception
{

}
