<?php

namespace TestTaskFW\Config;

use TestTaskFW\Config\Exception;

/**
 * Простой конфиг для прилодения тестового задания
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Config
{
    /**
     * Расширение для файла конфига, добавляется к названию расширения
     */
    const CONFIG_FILE_EXT = '.config.php';
    /**
     * Директория для локальных конфигов (например на машинах разработчиков)
     */
    const LOCAL_CONFIG_DIR = '.local';

    /**
     * Путь к директории с кофигами
     * @var string
     */
    protected $configPath;

    /**
     * Загруженые конфиги
     * @var []
     */
    private $configList = [];

    /**
     *
     * @param string путь к директории с кофигами
     */
    public function __construct($configPath)
    {
        $this->configPath = $configPath;
    }

    /**
     * Возвращает конфиг по имени, если нужно подгрузив его из файлов
     * @param string $configName
     * @return []
     */
    public function get($configName)
    {
        if (!isset($this->configList[$configName])) {
            $this->configList[$configName] = $this->loadConfig($configName);
        }
        return $this->configList[$configName];
    }

    /**
     * Загружает конфиги из файлов
     * @param string $configName имя конфига,
     * соотвествует файлу $configName.self::CONFIG_FILE_EXT в директории переданой в конструктор
     * @return array
     * @throws Exception\ConfigNotExistException если конфиг с именем $configName не существует
     * @throws Exception\IncorrectConfigDataException если файл вернул что-то отличное от массива
     */
    protected function loadConfig($configName)
    {
        $configFile = $configName . self::CONFIG_FILE_EXT;
        if (!file_exists($this->configPath . DIRECTORY_SEPARATOR . $configFile)) {
            throw new Exception\ConfigNotExistException("Config \"$configName\" not found");
        }

        $config = require_once $this->configPath . DIRECTORY_SEPARATOR . $configFile;

        if (!is_array($config)) {
            throw new Exception\IncorrectConfigDataException("Config file '{$configFile}' return not array value");
        }

        $configLocalFile = self::LOCAL_CONFIG_DIR . DIRECTORY_SEPARATOR . $configFile;

        if (file_exists($this->configPath . DIRECTORY_SEPARATOR . $configLocalFile)) {
            $configLocal = require_once $this->configPath . DIRECTORY_SEPARATOR . $configLocalFile;

            if (!is_array($configLocal)) {
                throw new Exception\IncorrectConfigDataException("Config file '{$configFile}' return not array value");
            }

            $config = array_replace_recursive($config, $configLocal);
        }

        return $config;
    }
}
