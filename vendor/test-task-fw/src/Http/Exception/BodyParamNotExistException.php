<?php

namespace TestTaskFW\Http\Exception;

/**
 * Бросается при попытке получить несуществующие значение из тела запроса
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class BodyParamNotExistException extends HttpException
{

}
