<?php

namespace TestTaskFW\Http\Exception;

use Exception;

/**
 * Базовое исключение для Http
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class HttpException extends Exception
{

}
