<?php

namespace TestTaskFW\Http\Exception;

/**
 * Бросается при попытке получить несуществующие значение из параметров запроса
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class QueryParamNotExistException extends HttpException
{

}
