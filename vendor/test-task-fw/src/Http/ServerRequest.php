<?php

namespace TestTaskFW\Http;

use TestTaskFW\Http\Exception;

/**
 * Запрос к серверу
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ServerRequest
{
    const AJAX_HEADER = 'HTTP_X_REQUESTED_WITH';
    const AJAX_HEADER_VALUE = 'XMLHttpRequest';
    /**
     * Тело запрса в виде массива, из $_POST
     * @var []
     */
    private $body;
    /**
     * Параметры запроса в виде массива из $_GET
     * @var []
     */
    private $query;

    /**
     *
     * @param array $query параметры запроса в виде массива, если не задано, то берется из $_GET
     * @param array $body тело запроса в виде массива, если не задано, то берется из $_POST
     */
    public function __construct(array $query = null, array $body = null)
    {
        $this->query = $query? : $_GET;
        $this->body = $body? : $_POST;
    }

    /**
     * Возвращает тело запроса в виде массива ($_POST)
     * @return array
     */
    public function getBodyArray()
    {
        return $this->body;
    }

    /**
     * Возвращает параметр из тела запроса по имени $name,
     * либо дефолтное заначение $default, если передано,
     * либо бросает исключение Exception\BodyParamNotExistException
     * @param string $name
     * @param mixed $default
     * @return mixed
     * @throws Exception\BodyParamNotExistException
     */
    public function getBodyValue($name, $default = null)
    {
        if (isset($this->body[$name])) {
            return $this->body[$name];
        }
        if ($default !== null) {
            return $default;
        }
        throw new Exception\BodyParamNotExistException("Request body param {$name} not exist");
    }

    /**
     * Возвращает параметры запроса ($_GET) в виде массива
     * @return array
     */
    public function getQueryArray()
    {
        return $this->query;
    }

    /**
     * Возвращает параметр запроса ($_GET) по имени $name,
     * либо дефолтное заначение $default, если передано,
     * либо бросает исключение Exception\QueryParamNotExistException
     * @param string $name
     * @param mixed $default
     * @return mixed
     * @throws Exception\QueryParamNotExistException
     */
    public function getQueryValue($name, $default = null)
    {
        if (isset($this->query[$name])) {
            return $this->query[$name];
        }
        if ($default !== null) {
            return $default;
        }
        throw new Exception\QueryParamNotExistException("Query param {$name} not exist");
    }

    public function isAjaxRequest()
    {
        return isset($_SERVER[self::AJAX_HEADER]) && $_SERVER[self::AJAX_HEADER] === self::AJAX_HEADER_VALUE;
    }
}
