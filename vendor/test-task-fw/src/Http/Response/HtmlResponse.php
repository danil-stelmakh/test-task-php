<?php

namespace TestTaskFW\Http\Response;

/**
 * Респонз для HTML данных
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class HtmlResponse extends AbstractResponse
{
    private $body = '';

    public function __construct($body = '')
    {
        $this->body = (string) $body;
        $this->setHeader('Content-Type', 'text/html');
    }

    public function getBody()
    {
        return $this->body;
    }
}
