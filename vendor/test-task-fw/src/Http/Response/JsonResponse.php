<?php

namespace TestTaskFW\Http\Response;

/**
 * Response для JSON ответа
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class JsonResponse extends AbstractResponse
{
    /**
     * Данные для ответа, преобразуемые в JSON
     */
    protected $responseData = [];

    public function __construct(array $responseData = [])
    {
        $this->responseData = $responseData;
        $this->setHeader('Content-Type', 'application/json');
    }

    public function getBody()
    {
        return json_encode($this->responseData);
    }
}
