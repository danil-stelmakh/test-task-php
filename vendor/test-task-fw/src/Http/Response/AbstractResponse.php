<?php

namespace TestTaskFW\Http\Response;

/**
 * Абстрактный http ответ
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
abstract class AbstractResponse
{
    const HTTP_STATUS_OK                = 200;
    const HTTP_STATUS_MOVED_PERMANENTLY = 301;
    const HTTP_STATUS_BAD_REQUEST       = 400;
    const HTTP_STATUS_FORBIDDEN         = 403;
    const HTTP_STATUS_NOT_FOUND         = 404;

    private static $statusPhrases = array(
        self::HTTP_STATUS_OK                => 'OK',
        self::HTTP_STATUS_MOVED_PERMANENTLY => 'Moved Permanently',
        self::HTTP_STATUS_BAD_REQUEST       => 'Bad Request',
        self::HTTP_STATUS_FORBIDDEN         => 'Forbidden',
        self::HTTP_STATUS_NOT_FOUND         => 'Not Found',
    );
    /**
     * Заголовки ответа
     * @var array
     * array([headerName => array(value, [values, ]), ] ... )
     */
    private $headerList = [];
    /**
     *
     */
    private $statusCode = self::HTTP_STATUS_OK;

    public function getStatusPhrase()
    {
        return self::$statusPhrases[$this->statusCode];
    }

    /**
     * Устанавливает код статуса ответа
     * @param int $code
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Устанавливает код статуса ответа
     * @param int $code
     */
    public function setStatusCode($code)
    {
        $this->statusCode = (int) $code;
    }

    /**
     * Заголовки ответа
     * @return array
     */
    public function getHeaders()
    {
        return $this->headerList;
    }

    /**
     * Задает значения заголовков ответа
     * @param string $headerName
     * @param mixed $value
     */
    public function setHeader($headerName, $value)
    {
        if (!isset($this->headerList[$headerName])) {
            $this->headerList[$headerName] = [];
        }
        $this->headerList[$headerName][] = $value;
    }

    /**
     * Возвращает тело запроса
     * @return string тело запроса
     */
    abstract public function getBody();

    /**
     * Выводит респонз
     */
    public function emit()
    {
        header("HTTP/1.1 {$this->statusCode} {$this->getStatusPhrase()}");
        foreach ($this->getHeaders() as $header => $valueList) {
            foreach ($valueList as $value) {
                header("{$header}: {$value}", false);
            }
        }
        echo $this->getBody();
    }
}
