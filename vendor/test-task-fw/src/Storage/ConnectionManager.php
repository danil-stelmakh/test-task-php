<?php

namespace TestTaskFW\Storage;

use PDO;
use TestTaskFW\Config\Config;

/**
 * Менедер соеденений
 * @author Danilka
 * @method ConnectionManager getInstance() Получит инстанс объекта (синглтон)
 */
class ConnectionManager
{
    /**
     * Конфиг для соединений PDO
     */
    const PDO_CONFIG = 'pdo';

    /**
     * Конфиг приложения
     * @var Config
     */
    protected $config;
    /**
     * Пул соединений с базами по их наименованию в конфиге pdo.config.db
     * @var PDO[]
     */
    protected $pdo = [];

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Возвращает соединение с базой данных по наименованию соединения из конфига pdo.config.php
     * @param string $name наименование соеденения
     * @return PDO
     */
    public function getPdo($name)
    {
        if (!isset($this->pdo[$name])) {
            $this->pdo[$name] = $this->makePdo($name);
        }
        return $this->pdo[$name];
    }

    /**
     * Созадет новое соеденение с параметрами получеными из конфига pdo.config.db по имени соединения
     * @param string $name наименование
     * @return PDO
     */
    private function makePdo($name)
    {
        $config = $this->config->get(self::PDO_CONFIG)[$name];
        return new PDO($config['dsn'], $config['username'], $config['password'], $config['options']);
    }
}
