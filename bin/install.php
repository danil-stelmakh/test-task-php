#!/usr/bin/env php
<?php

use TestTaskFW\Config\Config;
use TestTaskFW\Storage\ConnectionManager;


require_once realpath(__DIR__ . '/../bootstrap/init.php');

$pdo = (new ConnectionManager(new Config(CONFIG_PATH)))->getPdo('main');

$pdo->query('
CREATE TABLE "order" (
	id SERIAL PRIMARY KEY,
	name VARCHAR(255),
	email VARCHAR(255),
	phone VARCHAR(12),
	card VARCHAR(255),
	token VARCHAR(32)
)');

$pdo->query('
CREATE TABLE order_items (
    "order" integer NOT NULL,
    item integer NOT NULL,
    cost double precision NOT NULL,
    amount integer NOT NULL,
    CONSTRAINT order_items_pkey PRIMARY KEY ("order", item)
)');

$res = $pdo->query('
CREATE TABLE items (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    cost double precision NOT NULL
)');

$st = $pdo->prepare("INSERT INTO items VALUES (DEFAULT, :title, :cost)");
// Забиваем товарами для теста
for ($i = 1; $i <= 1000; $i++) {
    $cost = mt_rand(1, 100) * 100 + mt_rand(1, 9) / 10;
    $title = "Товар №{$i}";
    $st->bindValue('cost', $cost);
    $st->bindValue('title', $title);
    $st->execute();
}
