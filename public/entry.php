<?php

use TestTaskFW\Config\Config;
use TestTaskFW\App\Application;
use App\Controller\AppController;

require_once './../bootstrap/init.php';

(new AppController(new Application(new Config(CONFIG_PATH))))->run();
