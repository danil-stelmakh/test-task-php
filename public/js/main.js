// Namespaces
var TestTask = {
    Widget: {},
    Page: {}
};

/**
 * Показывает уведомление нужного типа вверху страницы
 * @param {string} type (error|info|success)
 * @param {type} title заголовок
 * @param {type} message текст
 * @returns {PNotify}
 */
TestTask.Widget.TopStackBar = function (type, title, message) {
    var options = {
        title: title,
        text: message,
        type: type,
        styling: 'bootstrap3',
        delay: 3000,
        buttons: {
            sticker: false
        }
    };
    return new PNotify(options);
};

/**
 * Главная страница
 */
TestTask.Page.Index = {
    run: function () {
        TestTask.Request('page.getIndex', null, function (response) {
            TestTask.Page.Index.show(response);
        });
    },
    destroy: function () {
    },
    show: function (response) {
        var html = '<h1>Главная страница</h1>';
        html += '<div class="container"><div class="row">';
        jQuery.each(response.itemList, function (index, item) {
            html += '<div class="col-md-4">';
            html += '<h2>' + item.title + '</h2>';
            html += '<image src=' + item.image_url + ' style="float:left;"/>';
            html += '<p>' + item.description + '</p>';
            html += '<h4>Цена: <strong>' + (1 * item.cost).toFixed(2) + '</strong> руб.</h4>';
            html += '<p><a class="btn btn-default add_to_cart" href="#" role="button" rel="' + item.id + '">Купить</a></p>';
            html += '</div>';
        });
        html += '</div></div>';
        jQuery('#content').html(html);
        jQuery('#content .add_to_cart').click(function () {
            TestTask.Cart.incr(jQuery(this).attr('rel'), 1);
            return false;
        })
    }
};


/**
 * Страница оформеления заказа
 */
TestTask.Page.Checkout = {
    run: function () {
        jQuery('#content').html('<h1>Оформеление заказа</h1>');
    },
    destroy: function () {
    }
};


/**
 * Корзина
 */
TestTask.Cart = {
    amount: 0,
    cost: 0,
    itemList: [],
    incr: function (id, incr) {
        TestTask.Request('cart.changeItem', {id: id, incr: incr}, function (response) {

        });
    },
    remove: function (id) {
        TestTask.Request('cart.changeItem', {id: id, incr: 0}, function (response) {

        });
    },
    show: function () {
        if (this.amount > 0) {
            jQuery('#cart-info').html(this.amount + ' товаров на сумму ' + this.cost.toFixed(2) + ' руб.');
            jQuery('#cart-checkout').show();
            this.makeItemTable();
        } else {
            jQuery('#cart-info').html('Корзина пуста');
            jQuery('#cart-content').html('Корзина пуста');
            jQuery('#cart-checkout').hide();
        }
    },
    makeItemTable: function () {
        var html = '';
        html += '<table cellspacing="0" class="table table-striped table-bordered" id="example">';
        html += '<thead><tr>';
        html += '<th>#</th>';
        html += '<th>Товар</th>';
        html += '<th>Цена (руб.)</th>';
        html += '<th>Кол-во</th>';
        html += '<th>Сумма (руб.)</th>';
        html += '<th></th>';
        html += '</tr></thead>';
        jQuery.each(this.itemList, function (index, itemData) {
            html += '<tr>';
            html += '<td>' + (1 * index + 1) + '</td>';
            html += '<td>' + itemData.item.title + '</td>';
            html += '<td>' + (1 * itemData.item.cost).toFixed(2) + '</td>';
            html += '<td><button type="button" class="btn remove_from_cart" rel="' + itemData.item.id + '">-</button>';
            html += '<span class="well well-sm">' + itemData.amount + '</span>';
            html += '<button type="button" class="btn add_to_cart" rel="' + itemData.item.id + '">+</button></td>';
            html += '<td>' + (itemData.item.cost * itemData.amount).toFixed(2) + '</td>';
            html += '<td><button type="button" class="btn remove_all" rel="' + itemData.item.id + '">Убрать</button></td>';
            html += '</tr>';
        });
        html += '<tfoot><tr>';
        html += '<th></th>';
        html += '<th></th>';
        html += '<th><strong>Итого:</strong></th>';
        html += '<th>' + this.amount + '</th>';
        html += '<th>' + this.cost.toFixed(2) + '</th>';
        html += '<th></th>';
        html += '</tr></tfoot>';
        html += '</table>';
        jQuery('#cart-content').html(html);
        jQuery('#cart-content .add_to_cart').click(function () {
            TestTask.Cart.incr(jQuery(this).attr('rel'), 1);
            return false;
        });
        jQuery('#cart-content .remove_from_cart').click(function () {
            TestTask.Cart.incr(jQuery(this).attr('rel'), -1);
            return false;
        });
        jQuery('#cart-content .remove_all').click(function () {
            TestTask.Cart.incr(jQuery(this).attr('rel'), 0);
            return false;
        });
    },
    setData: function (data) {
        this.amount = data.amount;
        this.cost = data.cost * 1;
        this.itemList = data.itemList;
        this.show();
    }
}

TestTask.Request = function (commandName, data, callback) {
    jQuery.ajax({
        type: "POST",
        url: '/entry.php?cmd=' + commandName,
        data: data,
        dataType: 'json'
    }).done(function (data) {
        if (data.notify) {
            TestTask.Widget.TopStackBar(data.notify.type, data.notify.title, data.notify.message);
        }
        if (data.cart) {
            TestTask.Cart.setData(data.cart);
        }
        if (callback) {
            callback(data.response ? data.response : {});
        }
    }).fail(function (response) {
        if (response.responseJSON.error) {
            var message = response.responseJSON.error.message;
            console.error('ajax error:', response.responseJSON.error);
        } else {
            var message = 'Произошла ошибка при обращению к серверу.';
        }
        TestTask.Widget.TopStackBar('error', 'Ошибка', message);
    });
};

TestTask.App = {
    currentPage: false,
    init: function () {
        jQuery(".menu_link").click(function (e) {
            var pageName = jQuery(this).attr('rel');
            TestTask.App.runPage(pageName);
            return false;
        });
        jQuery('#cart-popover').click(function () {
            jQuery('#cart-modal').modal('show');
        });
        jQuery('#cart-checkout').click(function () {
            jQuery('#cart-modal').modal('hide');
            TestTask.App.runPage('Checkout');
        });
    },
    runPage: function (pageName) {
        if (this.currentPage) {
            this.currentPage.destroy();
        }
        this.currentPage = TestTask.Page[pageName];
        this.currentPage.run();
    },
    run: function () {
        this.init();
        this.runPage('Index');
    }
};

jQuery(function () {
    TestTask.App.run();
});
