<?php

return [
    'main' => [
        'dsn'      => 'pgsql:host=localhost;port=5432;dbname=testtaskdb;',
        'username' => 'danilka',
        'password' => 'testpass',
        'options'  => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ],
    ],
];
