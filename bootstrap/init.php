<?php

mb_internal_encoding('UTF-8');

/**
 * Разделитель пространства имен
 */
define('NS_SEPARATOR', '\\');

/**
 * Время на момент запуска скрипта, можно использовать вместо time(),
 * если предпологается что запрос выполняется не сильно больше секунды, и высокая точность не нужна
 */
define('REQUEST_TIME', empty($_SERVER['REQUEST_TIME']) ? time() : $_SERVER['REQUEST_TIME']);

define('ROOT_PATH', realpath(__DIR__ . '/../') . '/');
define('CONFIG_PATH', realpath(ROOT_PATH . 'config/') . '/');
define('PUBLIC_PATH', realpath(ROOT_PATH . 'public/') . '/');
define('VENDOR_PATH', realpath(ROOT_PATH . 'vendor/') . '/');

switch (isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : false) {
    case 'dev':
        define('IS_PROD', false);
        break;
    case 'production':
    default :
        define('IS_PROD', true);
}

require_once realpath(VENDOR_PATH . '/autoload.php');
require_once realpath(__DIR__ . '/error-policy.php');


