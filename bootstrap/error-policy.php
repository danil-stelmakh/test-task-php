<?php

error_reporting(E_ALL);

if (IS_PROD) {
    ini_set('display_errors', '0');
} else {
    ini_set('display_errors', '1');
}

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    switch ($errno) {
        case E_NOTICE:
        case E_RECOVERABLE_ERROR:
            throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
    return false;
});
