<?php

namespace App\Goods;

use JsonSerializable;

/**
 * Айтет-товар магазина
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Item implements JsonSerializable
{
    /**
     *
     * @var Application
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data                = $data;
        //Описание и картинка тоже должны приходить из БД, но для тестовой задачи сделаем так
        $this->data['image_url']   = './images/item.png';
        $this->data['description'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
                . ' Sed suscipit justo augue, vitae dictum dui euismod sit amet. '
                . 'Vivamus lacinia ligula libero, ac sollicitudin ipsum scelerisque nec. '
                . 'Quisque vitae semper est. Praesent arcu nisi, efficitur in risus fermentum, '
                . 'sagittis eleifend augue. Ut vehicula tristique erat, et tincidunt velit aliquam non. '
                . 'Fusce nec purus at dui iaculis placerat. In blandit vulputate arcu ac tempus. '
                . 'In vel dolor orci. Vestibulum justo odio, facilisis vitae turpis eget, '
                . 'scelerisque ullamcorper nulla. ';
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getCost()
    {
        return $this->data['cost'];
    }

    public function getTitle()
    {
        return $this->data['title'];
    }

    public function getDescription()
    {
        return $this->data['description'];
    }

    public function getImageUrl()
    {
        return $this->data['image_url'];
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}
