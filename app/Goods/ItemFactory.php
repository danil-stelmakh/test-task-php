<?php

namespace App\Goods;

use PDO;
use TestTaskFW\App\Application;
use App\Goods\Exception;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ItemFactory
{

    /**
     *
     * @param Application $app
     * @param int $count
     * @return \App\Goods\Item[]
     */
    public static function getRandomItemList(Application $app, $count)
    {
        $pdo = $app->getConnectionManager()->getPdo('main');
        /**
         * При большом кол-ве товаров, такой запрос будет медленным
         * как варинат, если мало пропусков ID, для ускорения можно генерить рандомные ID
         * от минимального до максимального
         * и выбирать уже конкртные, до достижения нужного кол-ва
         * если же распределени ID ближе к случайному, то имеет смысл сложить из все
         * во множество в редисе, и выдергивать случайные командой srandmember
         * @link http://redis.io/commands/srandmember
         */
        $result = $pdo->query('SELECT * FROM "items" ORDER BY RANDOM() LIMIT ' . (int) $count);
        $list = [];
        foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $itemData) {
            $list[] = new Item($itemData);
        }
        return $list;
    }

    /**
     * Возвращает айтем по ID
     * @param Application $app
     * @param int $id
     * @return \App\Goods\Item
     * @throws Exception\ItemNotFoundException
     */
    public static function getItemById(Application $app, $id)
    {
        $pdo = $app->getConnectionManager()->getPdo('main');
        $result = $pdo->query('SELECT * FROM "items" WHERE id=' . (int) $id);
        if ($result->rowCount() === 0) {
            throw new Exception\ItemNotFoundException('item id:' . (int) $id . ' not found');
        }
        return new Item($result->fetch(PDO::FETCH_ASSOC));
    }
}
