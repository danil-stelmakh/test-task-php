<?php

namespace App\Goods\Exception;

use Exception;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ItemException extends Exception
{
    
}
