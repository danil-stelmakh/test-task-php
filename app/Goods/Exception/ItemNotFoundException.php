<?php

namespace App\Goods\Exception;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class ItemNotFoundException extends ItemException
{
   
}
