<?php

namespace App\Controller;

use Exception;
use LogicException;
use TestTaskFW\Controller\AbstractCommand;
use TestTaskFW\Controller\AbstractController;
use TestTaskFW\Http\Response;
use App\Cart;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class AppController extends AbstractController
{
    const AJAX_ERROR_KEY = 'error';
    const COMMAND_PARAM = 'cmd';
    const COMMAND_PREFIX = 'Command';

    /**
     * Корзина покупок
     * @var Cart;
     */
    private $cart;

    public function __construct(\TestTaskFW\App\Application $application)
    {
        parent::__construct($application);
        session_start();
    }

    public function getCommand()
    {
        $commandNameSpace = __NAMESPACE__ . NS_SEPARATOR . 'Command' . NS_SEPARATOR;
        $commandName = (string) $this->getApp()->getRequest()->getQueryValue(self::COMMAND_PARAM, '');
        $commandClass = str_replace('.', ' ', $commandName);
        $commandClass = $commandNameSpace . ucwords($commandClass) . self::COMMAND_PREFIX;
        $commandClass = str_replace(' ', NS_SEPARATOR, $commandClass);

        if (class_exists($commandClass) && is_subclass_of($commandClass, AbstractCommand::class)) {
            return new $commandClass($this);
        }

        throw new LogicException("Command '{$commandName}' not exist");
    }

    protected function emitErrorResponse(Exception $exception, $status = Response\AbstractResponse::HTTP_STATUS_BAD_REQUEST)
    {
        $errorData = array(
            'message' => $exception->getMessage(),
            'code'    => $exception->getCode(),
        );
        if ($this->getApp()->getRequest()->isAjaxRequest()) {
            $response = new Response\JsonResponse(array(self::AJAX_ERROR_KEY => $errorData));
        } else {
            $response = new Response\HtmlResponse('<pre>' . print_r($errorData, 1) . '</pre>');
        }
        $response->setStatusCode($status);
        $response->emit();
        exit();
    }

    public function emitResponse(array $responseData)
    {
        (new Response\JsonResponse(['response' => $responseData, 'cart' => $this->getCart()]))->emit();
    }

    /**
     * Корзина покупок
     * @return Cart
     */
    public function getCart()
    {
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = new Cart();
        }
        return $_SESSION['cart'];
    }
}
