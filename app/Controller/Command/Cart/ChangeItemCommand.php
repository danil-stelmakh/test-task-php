<?php

namespace App\Controller\Command\Cart;

use TestTaskFW\Controller\AbstractCommand;
use App\Goods\ItemFactory;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 * @method \App\Controller\AppController getController()
 */
class ChangeItemCommand extends AbstractCommand
{

    public function execute()
    {
        $app = $this->getController()->getApp();
        $itemId = $app->getRequest()->getBodyValue('id');
        $incr = (int) $app->getRequest()->getBodyValue('incr');
        $item = ItemFactory::getItemById($app, $itemId);
        $cart = $this->getController()->getCart();
        if ($incr === 0) {
            $cart->removeItem($itemId);
        } else {
            $cart->incrementItem($item, $incr);
        }
        $this->getController()->emitResponse([]);
    }
}
