<?php

namespace App\Controller\Command;

use TestTaskFW\Controller\AbstractCommand;
use TestTaskFW\Http\Response\JsonResponse;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class CheckoutCommand extends AbstractCommand
{

    public function execute()
    {
        $responseData = array();
        (new JsonResponse(['response' => $responseData]))->emit();
    }
}
