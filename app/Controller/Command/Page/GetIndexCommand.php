<?php

namespace App\Controller\Command\Page;

use TestTaskFW\Controller\AbstractCommand;
use App\Goods\ItemFactory;

/**
 *
 * @author Danil Stelamkh <domdruzei@gmail.com>
 * @method \App\Controller\AppController getController()
 */
class GetIndexCommand extends AbstractCommand
{

    public function execute()
    {
        $app = $this->getController()->getApp();
        $config = $app->getConfig()->get('app');

        $responseData = [
            'itemList' => ItemFactory::getRandomItemList($app, $config['index']['items_count']),
        ];
        $this->getController()->emitResponse($responseData);
    }
}
