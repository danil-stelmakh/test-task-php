<?php

namespace App;

use JsonSerializable;
use App\Goods\Item;

/**
 * Корзина
 * @author Danil Stelamkh <domdruzei@gmail.com>
 */
class Cart implements JsonSerializable
{
    private $itemList = [];

    public function setItem(Item $item, $amount)
    {
        $this->itemList[$item->getId()] = [
            'item'   => $item,
            'amount' => $amount,
        ];
    }

    public function incrementItem(Item $item, $incr)
    {
        if (!isset($this->itemList[$item->getId()])) {
            if ($incr < 1) {
                return;
            }
            $this->setItem($item, $incr);
        } else {
            $this->setItem($item, $this->itemList[$item->getId()]['amount'] + $incr);
        }
        if ($this->itemList[$item->getId()]['amount'] < 1) {
            $this->removeItem($item->getId());
        }
    }

    public function removeItem($id)
    {
        unset($this->itemList[$id]);
    }

    public function jsonSerialize()
    {
        return ['itemList' => array_values($this->itemList)] + $this->calculate();
    }

    public function calculate()
    {
        $amout = 0;
        $cost = 0;
        foreach ($this->itemList as $itemData) {
            $amout += $itemData['amount'];
            $cost += $itemData['item']->getCost() * $itemData['amount'];
        }
        return ['cost' => $cost, 'amount' => $amout];
    }
}
